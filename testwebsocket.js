var connect = require('connect');
connect.createServer(connect.static(__dirname)).listen(1337);

var ws = require('websocket.io');
var server = ws.listen(8888, function(){
    console.log('Start WebSocket Server');
});

var ledexec = function(){
    var spawn = require('child_process').spawn;
    var ledblink = spawn('./ledblink.sh');
    ledblink.stdout.on('data', function(data){
        console.log('stdout: ' + data);
    });
};

var sendToClient = function(data){
    server.clients.forEach(function(client){
        if(client){
            client.send(data);
        }
    });
}

var pswexec = function(){
    var spawn = require('child_process').spawn;
    var pushsw = spawn('./pushsw.sh');
    pushsw.stdout.on('data', function(data){
        console.log('stdout: ' + data);
        if(data == 0){
            sendToClient("Push On!");
        }else if(data == 1){
            sendToClient("Release");
        }else{
            sendToClient("?");
        }
    });
};
pswexec();

server.on('connection', function(socket){
    socket.on('message', function(data){
        console.log('Receive: ' + data);
        ledexec();

        server.clients.forEach(function(client){
            if(client){
                client.send(data);
            }
        });
    });
});
