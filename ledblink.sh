#!/bin/sh
echo "17" > /sys/class/gpio/exprt
echo "out" > /sys/class/gpio/gpio17/direction

for i in `seq 1 1 3`
do
    # LED ON
    echo "1" > /sys/class/gpio/gpio17/value
    sleep 1
    # LED OFF
    echo "0" > /sys/class/gpio/gpio17/value
    sleep 1
    done
exit 0
